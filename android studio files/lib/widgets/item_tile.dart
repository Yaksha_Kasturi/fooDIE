import 'package:flutter/material.dart';
import 'package:foodie/models/item_data.dart';
import 'package:provider/provider.dart';

class ItemTile extends StatelessWidget {
  final Color expColor;
  final String itemName;
  final String docName;
  final Function onPress;

  ItemTile({this.expColor, this.itemName, this.docName, this.onPress});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding:
          EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
      leading: Padding(
        padding: const EdgeInsets.only(left: 0.0),
        child: Text(
          'EXP',
          style: TextStyle(
            color: expColor,
            fontSize: 18.0,
          ),
        ),
      ),
      title: Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Text(
          itemName,
          style: TextStyle(
            fontSize: 23.0,
          ),
        ),
      ),
      trailing: GestureDetector(
        onTap: onPress,
        child: Icon(
          Icons.delete_outline,
          color: Colors.red[400],
          size: 32.0,
        ),
      ),
    );
  }
}
