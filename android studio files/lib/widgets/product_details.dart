import 'networking.dart';

var apiKey = '0si5jt11nhidrbwc4pjgfc0obpfhg4';
var query = '8901052087440'; // tetley

class ProductDetails {
  String getURL(barcodeNumber) {
    var url =
        'https://api.barcodelookup.com/v2/products?barcode=$barcodeNumber&formatted=y&key=$apiKey';
    return url;
  }

  Future<dynamic> thisIsAFunction(url) async {
    NetworkHelper networkHelper = NetworkHelper(url);

    var productData = await networkHelper.getData();

    return productData;
  }
}

//products[0].product_name
