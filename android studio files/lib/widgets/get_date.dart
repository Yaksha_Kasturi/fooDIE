import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:foodie/models/item_data.dart';
import 'package:provider/provider.dart';

DateTime expiryDate;
DateTime currentDate = DateTime.now();

class GetDate extends StatefulWidget {
  @override
  _GetDateState createState() => _GetDateState();
}

class _GetDateState extends State<GetDate> {
  String finalDate = '';
  final _myController = TextEditingController();

  void setExpiry() {
//    var date = expiryDate.toString();
//    var dateParse = DateTime.parse(date);
//    var formattedDate1 =
//        '${dateParse.day} - ${dateParse.month} - ${dateParse.year}';

    var formattedDate =
        '${expiryDate.day} - ${expiryDate.month} - ${expiryDate.year}';
    _myController.text = formattedDate;
    _myController.selection =
        TextSelection.collapsed(offset: formattedDate.length);
    Provider.of<ItemData>(context, listen: false).setItemExpiry(expiryDate);
    // here we can use expiryDate to compare to current date for sending notifications
  }

  @override
  void dispose() {
    _myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container(
          child: Column(
            children: [
              ListTile(
                title: TextFormField(
                  key: Key('date'),
                  controller: _myController,
                  decoration: InputDecoration(labelText: 'Expiry Date'),
                ),
                trailing: GestureDetector(
                  onTap: () {
                    CupertinoRoundedDatePicker.show(
                      context,
                      initialDate: DateTime.now(),
                      minimumYear: 2020,
                      maximumYear: 2040,
                      fontFamily: "Exo",
                      textColor: Colors.white,
                      background: Colors.lightGreen[600],
                      borderRadius: 16,
                      initialDatePickerMode: CupertinoDatePickerMode.date,
                      onDateTimeChanged: (newDateTime) {
                        expiryDate = newDateTime;
                        setExpiry();
                      },
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 12.0,
                      bottom: 15.0,
                      left: 20.0,
                      right: 20.0,
                    ),
                    child: Icon(
                      Icons.date_range,
                      color: Colors.white,
                      size: 28.0,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.lightGreen,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    //margin: EdgeInsets.all(10.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
