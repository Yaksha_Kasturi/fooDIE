import 'package:provider/provider.dart';
import 'product_details.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:foodie/models/item_data.dart';

var barcodeValue;
var productURL = ' ';
var productName;
String textTitle = 'Product Name';

class ScanBarcode extends StatefulWidget {
  @override
  _ScanBarcodeState createState() => _ScanBarcodeState();
}

class _ScanBarcodeState extends State<ScanBarcode> {
  final _myController = TextEditingController();

  ProductDetails product = ProductDetails();

  Future<String> getBarcode() async {
    var result = await BarcodeScanner.scan();
    var barcodeContent = result.rawContent.toString();
    return barcodeContent;
  }

  void getProductDetails() async {
    var productDetails = await ProductDetails().thisIsAFunction(productURL);
    productName = productDetails['products'][0]['product_name'];
    _myController.text = productName;
    _myController.selection =
        TextSelection.collapsed(offset: productName.length);
    Provider.of<ItemData>(context, listen: false).setItemName(productName);
    print(productName);
  }

  void finalFunction() async {
    barcodeValue = await getBarcode();
    productURL = product.getURL(barcodeValue);
    getProductDetails();
  }

//  @override
//  void initState() {
//    textTitle = productName;
//    super.initState();
//  }

  @override
  void dispose() {
    _myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 50.0),
              ListTile(
                title: TextFormField(
                  key: Key('name'),
                  controller: _myController,
                  decoration: InputDecoration(labelText: 'Product Name'),
                  onFieldSubmitted: (newName) {
                    Provider.of<ItemData>(context, listen: false)
                        .setItemName(newName);
                  },
                ),
                trailing: GestureDetector(
                  onTap: () {
                    setState(() {
                      finalFunction();
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 12.0,
                      bottom: 15.0,
                      left: 18.0,
                      right: 18.0,
                    ),
                    child: Icon(
                      Icons.scanner,
                      color: Colors.white,
                      size: 30.0,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.lightGreen,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    //margin: EdgeInsets.all(10.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
