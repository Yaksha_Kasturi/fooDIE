import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodie/widgets/item_list.dart';
import 'package:foodie/widgets/item_tile.dart';

class Item {
  final String name;
  final DateTime expiry;

  Item({this.name, this.expiry});
}

class Items extends StatefulWidget {
  static const id = 'item';

  @override
  _ItemsState createState() => _ItemsState();
}

class _ItemsState extends State<Items> {
  final databaseRef = Firestore.instance;
  var documentName;

  void getName() async {
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    documentName = firebaseUser.email;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: databaseRef.collection(documentName).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) return new Text('${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return new Center(child: Text('Loading...'));
          default:
            return new ListView(children: [
              ItemTile(
                itemName: 'hello there',
                expColor: Colors.green,
              ),
            ]);
        }
      },
    );
  }
}
