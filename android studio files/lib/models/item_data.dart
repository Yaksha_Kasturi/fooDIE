import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:foodie/screens/add_item_screen.dart';
import 'item.dart';
import 'dart:collection';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:async';

class ItemData extends ChangeNotifier {
  String itemName;
  var itemExpiry;
  final databaseReference = Firestore.instance;
  var collectionName;

  List<Item> _items = [];
  List<Item> _extras = [];
  List<Item> _branded = [];
  List<Item> _frozen = [];
  List<Item> _daily = [];
  List<Item> _bakery = [];
  List<Item> _beverages = [];

  String get iName {
    return itemName;
  }

  dynamic get iExpiry {
    return itemExpiry;
  }

  void setItemExpiry(expiry) {
    itemExpiry = expiry;
    notifyListeners();
  }

  void setItemName(name) {
    itemName = name;
    notifyListeners();
  }

  UnmodifiableListView<Item> get items {
    return UnmodifiableListView(_items);
  }

  UnmodifiableListView<Item> get extras {
    return UnmodifiableListView(_extras);
  }

  UnmodifiableListView<Item> get branded {
    return UnmodifiableListView(_branded);
  }

  UnmodifiableListView<Item> get frozen {
    return UnmodifiableListView(_frozen);
  }

  UnmodifiableListView<Item> get beverages {
    return UnmodifiableListView(_beverages);
  }

  UnmodifiableListView<Item> get bakery {
    return UnmodifiableListView(_bakery);
  }

  UnmodifiableListView<Item> get daily {
    return UnmodifiableListView(_daily);
  }

  int get itemCount {
    return _items.length;
  }

  int get extrasCount {
    return _extras.length;
  }

  int get brandedCount {
    return _branded.length;
  }

  int get frozenCount {
    return _frozen.length;
  }

  int get beveragesCount {
    return _beverages.length;
  }

  int get bakeryCount {
    return _bakery.length;
  }

  int get dailyCount {
    return _daily.length;
  }

  void addItem(String itemName, var itemExpiry, String docName) {
    final item = Item(name: itemName, expiry: itemExpiry);
    if (docName == 'Extras') {
      _extras.sort((a, b) => a.expiry.compareTo(b.expiry));
      _extras.add(item);
    } else if (docName == 'Branded Food') {
      _branded.add(item);
    } else if (docName == 'Beverages') {
      _beverages.add(item);
    } else if (docName == 'Frozen Food') {
      _frozen.add(item);
    } else if (docName == 'Bakery and Dairy') {
      _bakery.add(item);
    } else if (docName == 'Daily Staples') {
      _daily.add(item);
    }
    _items.add(item);
    notifyListeners();
  }

  void deleteItem(Item item, String docName) {
    if (docName == 'Extras') {
      _extras.remove(item);
    } else if (docName == 'Branded Food') {
      _branded.remove(item);
    } else if (docName == 'Beverages') {
      _beverages.remove(item);
    } else if (docName == 'Frozen Food') {
      _frozen.remove(item);
    } else if (docName == 'Bakery and Dairy') {
      _bakery.remove(item);
    } else if (docName == 'Daily Staples') {
      _daily.remove(item);
    }
    _items.remove(item);
    notifyListeners();
  }

  void addItemToFirebase(
      String itemName, var itemExpiry, String documentName) async {
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    final item = Item(name: itemName, expiry: itemExpiry);
    await databaseReference
        .collection(firebaseUser.email)
        .document(documentName)
        .collection('Items')
        .document('$itemName')
        .setData({});
    await databaseReference
        .collection(firebaseUser.email)
        .document(documentName)
        .collection('Items')
        .document('$itemName')
        .updateData({
      'item': item.name,
      'expiry': item.expiry,
    });
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid = AndroidInitializationSettings('app_logo');
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

  Future<void> notificationAfterSec(
      DateTime expiry, String expiryMessage, Duration scheduled) async {
    var scheduledTime = expiry.subtract(scheduled);
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
            'second channel ID', 'second Channel title', 'second channel body',
            icon: 'app_logo',
            color: const Color.fromARGB(255, 100, 253, 110),
            largeIcon: DrawableResourceAndroidBitmap('app_logo'),
            enableLights: true,
            priority: Priority.High,
            importance: Importance.Max,
            ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
        NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.schedule(
      1,
      'fooDIE',
      '$expiryMessage',
      scheduledTime,
      notificationDetails,
    );
  }

  void scheduleNotification(
      DateTime expiry, Duration scheduled, String expiryMessage) async {
    await notificationAfterSec(expiry, expiryMessage, scheduled);
  }
}
