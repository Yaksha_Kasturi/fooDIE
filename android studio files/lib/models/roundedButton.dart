import 'package:flutter/material.dart';
//import 'package:flash_chat/screens/welcome_screen.dart';

// ignore: must_be_immutable
class RoundedButton extends StatelessWidget {
  RoundedButton({this.onPressed, this.buttonColor, this.buttonText});

  Color buttonColor;
  String buttonText;
  Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        elevation: 5.0,
        color: buttonColor,
        borderRadius: BorderRadius.circular(50.0),
        child: MaterialButton(
          onPressed: onPressed,
          minWidth: 300.0,
          height: 45.0,
          child: Text(
            buttonText,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Exo',
              fontSize: 20.0,
              letterSpacing: 1.0,
            ),
          ),
        ),
      ),
    );
  }
}
