import 'package:flutter/material.dart';
import 'package:foodie/models/item_data.dart';
import 'package:provider/provider.dart';
import 'package:foodie/widgets/item_tile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodie/screens/add_item_screen.dart';
import 'package:foodie/widgets/item_list.dart';
import 'list_title.dart';
import 'package:foodie/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';

var collectionName;
final databaseReference = Firestore.instance;

class BrandedFoodItemScreen extends StatefulWidget {
  static const String id = 'branded_food';
  static const String docName = 'Branded Food';

  @override
  _BrandedFoodItemScreenState createState() => _BrandedFoodItemScreenState();
}

class _BrandedFoodItemScreenState extends State<BrandedFoodItemScreen> {
  final databaseReference = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen,
      floatingActionButton: FAB(docname: BrandedFoodItemScreen.docName),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: 15.0, left: 10.0, right: 30.0, bottom: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTitle(
                    title: BrandedFoodItemScreen.docName,
                    titleIcon: Icons.dashboard,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: kInnerContainer,
                child: BrandedFoodList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BrandedFoodList extends StatelessWidget {
  Color setColor(var itemExpiry) {
    DateTime currentDate = DateTime.now();
    Color expColor =
        currentDate.isBefore(itemExpiry) ? Colors.grey : Colors.red[400];
    return expColor;
  }

  void getCollectionName() async {
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    collectionName = firebaseUser.email;
  }

//  void getListData() {
//    databaseReference
//        .collection(collectionName)
//        .document('Branded Food')
//        .collection('Items')
//        .getDocuments()
//        .then((QuerySnapshot snapshot) {
//      snapshot.documents.forEach((f) => print('${f.data}}'));
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ItemData>(
      builder: (context, itemData, child) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final item = itemData.branded[index];
            return ItemTile(
              itemName: item.name,
              expColor: setColor(item.expiry),
              onPress: () {
                Provider.of<ItemData>(context, listen: false)
                    .deleteItem(item, 'Branded Food');
              },
            );
          },
          itemCount: itemData.brandedCount,
        );
      },
    );
  }
}
