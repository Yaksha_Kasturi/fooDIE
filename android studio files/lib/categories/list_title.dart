import 'package:flutter/material.dart';
import 'package:foodie/screens/add_item_screen.dart';

class ListTitle extends StatelessWidget {
  final IconData titleIcon;
  final String title;

  ListTitle({this.title, this.titleIcon});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          titleIcon,
          color: Colors.white,
          size: 30.0,
        ),
        SizedBox(width: 10.0),
        Text(
          title,
          style: TextStyle(
            fontSize: 30.0,
            fontFamily: 'Exo',
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class FAB extends StatelessWidget {
  final String docname;

  FAB({this.docname});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      foregroundColor: Colors.white,
      onPressed: () {
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          builder: (context) => SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: AddItemScreen(
                documentName: docname,
              ),
            ),
          ),
        );
      },
      backgroundColor: Colors.lightGreen,
      child: Icon(Icons.add),
    );
  }
}
