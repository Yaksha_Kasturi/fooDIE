import 'package:flutter/material.dart';
import 'package:foodie/models/item_data.dart';
import 'package:provider/provider.dart';
import 'package:foodie/widgets/item_tile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'list_title.dart';
import 'package:foodie/constants.dart';

class DailyStaplesList extends StatelessWidget {
  Color setColor(var itemExpiry) {
    DateTime currentDate = DateTime.now();
    Color expColor =
        currentDate.isBefore(itemExpiry) ? Colors.grey : Colors.red[400];
    return expColor;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ItemData>(
      builder: (context, itemData, child) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final item = itemData.daily[index];
            return ItemTile(
              itemName: item.name,
              expColor: setColor(item.expiry),
              onPress: () {
                Provider.of<ItemData>(context, listen: false)
                    .deleteItem(item, 'Daily Staples');
              },
            );
          },
          itemCount: itemData.dailyCount,
        );
      },
    );
  }
}

class DailyStaplesItemScreen extends StatefulWidget {
  static const String id = 'daily_staples';
  static const String docName = 'Daily Staples';

  @override
  _DailyStaplesItemScreenState createState() => _DailyStaplesItemScreenState();
}

class _DailyStaplesItemScreenState extends State<DailyStaplesItemScreen> {
  final databaseReference = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen,
      floatingActionButton: FAB(
        docname: DailyStaplesItemScreen.docName,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: 15.0, left: 10.0, right: 30.0, bottom: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTitle(
                    title: DailyStaplesItemScreen.docName,
                    titleIcon: Icons.shopping_cart,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: kInnerContainer,
                child: DailyStaplesList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
