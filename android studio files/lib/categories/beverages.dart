import 'package:flutter/material.dart';
import 'package:foodie/models/item_data.dart';
import 'package:provider/provider.dart';
import 'package:foodie/widgets/item_tile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodie/screens/add_item_screen.dart';
import 'package:foodie/widgets/item_list.dart';
import 'list_title.dart';
import 'package:foodie/constants.dart';

class BeveragesList extends StatelessWidget {
  Color setColor(var itemExpiry) {
    DateTime currentDate = DateTime.now();
    Color expColor =
        currentDate.isBefore(itemExpiry) ? Colors.grey : Colors.red[400];
    return expColor;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ItemData>(
      builder: (context, itemData, child) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final item = itemData.beverages[index];
            return ItemTile(
              itemName: item.name,
              expColor: setColor(item.expiry),
              onPress: () {
                Provider.of<ItemData>(context, listen: false)
                    .deleteItem(item, 'Beverages');
              },
            );
          },
          itemCount: itemData.beveragesCount,
        );
      },
    );
  }
}

class BeveragesItemScreen extends StatefulWidget {
  static const String id = 'beverages';
  static const String docName = 'Beverages';

  @override
  _BeveragesItemScreenState createState() => _BeveragesItemScreenState();
}

class _BeveragesItemScreenState extends State<BeveragesItemScreen> {
  final databaseReference = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen,
      floatingActionButton: FAB(
        docname: BeveragesItemScreen.docName,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: 15.0, left: 10.0, right: 30.0, bottom: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTitle(
                    title: BeveragesItemScreen.docName,
                    titleIcon: Icons.local_bar,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: kInnerContainer,
                child: BeveragesList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
